[![pipeline status](https://gitlab.com/amberkowalski/rust-hello-world/badges/master/pipeline.svg)](https://gitlab.com/amberkowalski/rust-hello-world/commits/master)


# Simple, Rust `Hello, World!`
I made this to get used to rusty syntax, cargo, git (yeah I still suck at git but I try), and all of rust's **amazing*** tooling. Tests, also.


### Wait this isn't simple at all
No, no its not.


### You should put this on Crates.io
Nah, thanks for the suggestion though.


### Can I contribute?
Why?
