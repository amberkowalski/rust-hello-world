mod starter {
    use rand::Rng;
    use std::string::String;

    pub struct HelloWorld {
        pub greeting: String,
    }

    impl HelloWorld {
        /// Lets you change the greeting, give it a str.
        /// It gives you your string back but do you really want it?
        pub fn change_greeting(&mut self, new_greeting: &str) {
            self.greeting = String::from(new_greeting);
        }

        /// Prints out a typical greeting
        pub fn print_greeting(&self) {
            println!("{}, world!", self.greeting);
        }

        /// Returns the curren `HelloWorld` greeting
        /// Returns a borrowed string, please make sure to give it back q.q
        pub fn get_greeting(&self) -> &String {
            &self.greeting
        }
    }

    pub struct Random {
        // Hewwo, I'm empty
    }

    impl Random {
        /// Generates a random number.
        /// Returns a `u128`
        pub fn get_random(&self) -> Result<u128, &str> {
            let mut rng = rand::thread_rng();

            if rng.gen_range(0, 10) == 9 {
                Err("Looks like you got unlucky")
            } else {
                Ok(4)
            }
        }
    }
}

fn main() {
    // Lets get our helpers in
    use starter::{HelloWorld, Random};
    use std::{thread, time};

    // Create a HelloWorld object
    let mut hello_world = HelloWorld {
        greeting: String::from("hola"),
    };

    hello_world.print_greeting();

    hello_world.change_greeting("UwU, Hewwo");

    println!("{}, world!", hello_world.get_greeting());

    // Lets generate and print some random numbers

    let random_gen = || {
        let random_gen = Random {};
        for _ in 0..10 {
            match random_gen.get_random() {
                Err(_) => println!("Failed to get random number"),
                Ok(n) => println!("{}", n),
            };
            thread::sleep(time::Duration::from_secs(1));
        }
    };

    let rand_thread = thread::spawn(random_gen);

    rand_thread.join().unwrap();
}

#[cfg(test)]
mod test {
    use crate::starter::HelloWorld;
    use std::string::String;

    /// Sets up the HelloWorld object for tests
    fn hello_world_setup() -> HelloWorld {
        HelloWorld {
            greeting: String::from("test"),
        }
    }

    /// Tests if the `HelloWorld` struct is created properly
    #[test]
    fn hello_world_struct_creation() {
        let hello_world = hello_world_setup();
        assert_eq!(hello_world.greeting, "test");
    }

    /// Tests if the get_greeting method works
    #[test]
    fn hello_world_get_greeting() {
        let hello_world = hello_world_setup();
        assert_eq!(hello_world.greeting, "test");
    }

    /// Tests if the change_greeting method works
    #[test]
    fn hello_world_change_greeting() {
        let mut hello_world = hello_world_setup();
        hello_world.change_greeting("test2");
        assert_eq!(hello_world.greeting, "test2");
    }
}
